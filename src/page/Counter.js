import React, { Component } from 'react'

export class Counter extends Component {
  constructor(props){
    super(props);
    this.state = {
      count: 0
    }
  }
  render() {
    let increasedCount = ()=>{
      this.setState({
        count: this.state.count +1
      })
    }
    let decreasedCount = ()=>{
      this.setState({
        count: (this.state.count>0)?this.state.count - 1:0
      })
    }
    return (
        <div>
        <h1 className="text-center">{this.state.count}</h1>
        <div className="text-center">
            <button className="btn btn-success" onClick={()=>increasedCount()}>Increase</button>
            <button disabled={this.state.count==0} className="btn btn-warning" onClick={()=>decreasedCount()}>Decrease</button>
        </div>
      </div>
    )
  }
}

export default Counter;